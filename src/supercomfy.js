// req: superagent, events, debug

//= core/request
//= core/db
//= core/server

function supercomfy(serverUrl, opts) {
    return new CouchServer(serverUrl, opts);
}