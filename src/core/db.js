function CouchDB(server, dbname) {
    this.server = server;
    this.dbname = dbname;

    // initialise the method overrides
    // method overrides are used to redirect database level operations to a different couchserver
    // this is particularly useful when you have replication configured and want to direct write
    // operations to the parent server
    this._overrides = {};
} // CouchDB

['get', 'post', 'put', 'del'].forEach(function(method) {
    CouchDB.prototype[method] = function(url, data, callback) {
        var overrideDB = this._overrides[method];
        
        // if the url parameter is actually data, then map arguments
        if (typeof url == 'object' && (! (url instanceof String))) {
            callback = data;
            data = url;
            
            url = data._id || '';
        }

        // if we have a db override for the specified method, then pass the request there
        if (overrideDB) {
            return overrideDB[method].call(overrideDB, url, data, callback);
        }
        // otherwise, pass on to our server
        else {
            return this.server[method].call(this.server, this.dbname + '/' + (url || ''), data, callback);
        }
    };
});

CouchDB.prototype.redirectWrites = function(targetDB) {
    var meshdb = this;
    
    if (targetDB) {
        debug('redirecting writes to: ' + targetDB.server.url + '/' + targetDB.dbname);

        ['put', 'del'].forEach(function(method) {
            meshdb._overrides[method] = targetDB;
        });
    }
    else {
        debug('resetting redirects');
        this._overrides = {};
    }
    
    return this;
};

CouchDB.prototype.info = function(callback) {
    this.server.get(this.dbname, callback);
};

CouchDB.prototype.list = function(opts, callback) {
    this.get('_all_docs', opts, callback);
};