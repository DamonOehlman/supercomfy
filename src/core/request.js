function CouchRequest(request) {
    var couchReq = this;
    
    // call the inherited constructor
    events.EventEmitter.call(this);
    
    // set headers
    
    request.end(function(res) {
        couchReq.parseResponse(res);
    });
} // CouchRequest

CouchRequest.prototype = new events.EventEmitter();
CouchRequest.prototype.constructor = CouchRequest;

CouchRequest.prototype.parseResponse = function(res) {
    
    var body, err;

    function _formatError(text) {
        
        var details, formatted;

        try {
            details = JSON.parse(text);
        }
        catch(e) {
        }
        
        if (details) {
            formatted = new Error(details.error);
            formatted.reason = details.reason;
        }
        else {
            formatted = new Error(text);
        }
        
        return formatted;
    }
    
    debug('received response', res);
    
    // if we don't have a response, then return
    if (!res) return;
    
    if (res.ok) {
        try {
            body = JSON.parse(res.text);
        }
        catch (e) {
            err = new Error('Unable to parse response');
        }
    }
    
    // if we have no status, then something went wrong
    if (! res.status) {
        err = new Error('No response');
    }
    
    this.emit(
        'end', 
        res.error ? _formatError(res.text) : err,
        body,
        res
    );
};