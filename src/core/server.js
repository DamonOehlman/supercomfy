var reTrailingSlash = /\/$/,
    _mappedMethods = {
        del: 'DELETE'
    };

// ## CouchServer definition

function CouchServer(url, opts) {
    this.url = (url || 'http://localhost:5984/').replace(reTrailingSlash, '') + '/';
    
    // ensure options have been provided
    opts = opts || {};
    
    // set server options
    this.jsonp = opts.jsonp;
    
    // initialise the db cache
    this._dbCache = {};
}

['get', 'post', 'put', 'del'].forEach(function(method) {
    CouchServer.prototype[method] = function(url, data, callback) {
        var targetUrl, sa, request;
        
        // handle no url or data, just a callback
        if (typeof url == 'function' && typeof data == 'undefined') {
            callback = url;
            url = '';
        }
        // handle url but no data
        else if (typeof data == 'function' && typeof callback == 'undefined') {
            callback = data;
            data = undefined;
        }
        
        // ensure the target url is formatted correctly
        targetUrl = this.url.replace(reTrailingSlash, '') + '/' + url;
        
        // create the request
        debug('making ' + method + ' request to: ' + targetUrl);
        
        // create the super agent request
        sa = superagent(_mappedMethods[method] || method, targetUrl);
        
        // add data if we have some
        if (data) {
            sa[method == 'del' || method == 'get' ? 'query' : 'send'](data);
        } 
        
        // create the couch request
        request = new CouchRequest(sa);
        
        // handle the end response
        if (callback) {
            request.on('end', callback);
        }
        
        return request;
    };
});

CouchServer.prototype.create = function(dbname, callback) {
    this.put(dbname, {}, callback);
};

CouchServer.prototype.remove = function(dbname, callback) {
    this.del(dbname, callback);
};

CouchServer.prototype.use = function(dbname, flushCache) {
    if (!this._dbCache[dbname] || flushCache) {
        this._dbCache[dbname] = new CouchDB(this, dbname);
    }
    
    return this._dbCache[dbname];
};

CouchServer.prototype.list = function(opts, callback) {
    this.get('_all_dbs', opts, callback);
};