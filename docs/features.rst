Features
========

SuperComfy has the following features either now or soon:

- Ability to work with both NodeJS and the browser
- Supercool functionality that allows you to redirect write operations (useful in replication scenarios)