SuperComfy
==========

SuperComfy is a lightweight CouchDB client for NodeJS and theoretically the browser (as it is built on top of the excellent `SuperAgent`__ library).

At this stage, SuperComfy is an **experimental project** and we would recommend using a more established library like `nano`__ at the moment for production deployments. 

__ https://github.com/visionmedia/superagent
__ https://github.com/dscape/nano

Documentation Contents
======================

.. toctree::
   :maxdepth: 2
   
   features
   getting-started
   browser



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

