From the Browser
================

At the moment, SuperComfy architecturally supports connecting from the browser.  No testing, however, has been done in this area as yet.  

Additionally, it is important to note that `cross domain restrictions`__ will prevent SuperComfy from successfully working with a Couch instance unless the CouchDB server is used to serve the ``supercomfy.js`` file.

For more information around this technique, both `CouchApp.org <http://couchapp.org/>`_ and `kan.so <http://kan.so/>`_ have some excellent information.

__ http://en.wikipedia.org/wiki/Same_origin_policy
__ https://issues.apache.org/jira/browse/COUCHDB-431

What about CORS?
----------------

There is `active development for CORS support in CouchDB`__ but nothing is available yet as far as I know.  

JSONP?
------

While JSONP support would provide some ability to get data across domains from Couch servers, I'm not sure I see the value in adding JSONP support.  CORS is the future.