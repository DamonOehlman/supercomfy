.. highlight:: javascript

Getting Started with SuperComfy
===============================

SuperComfy exposes a public API that is designed to feel similar if you have experience working with `nano`__.  Not because we are trying to lure you away from that wonderful little library, just because `Nuno`__ did such an excellent job with it.

__ https://github.com/dscape/nano
__ http://twitter.com/dscape

For instance, a server instance is created by simply calling::

    var couch = supercomfy('http://localhost:5984/');
    
You can then get a reference to a database by calling the familiar ``use`` method::

    var db = couch.use('testdb');

While similar convenience methods are supplied (``info``, ``list``, etc) the general interface for interacting with both the server and db **do** differ a little from the way nano works.

In SuperComfy, we try to maintain the mapping between the CouchDB REST API and the library.  For this reason, both server and database instances expose the following methods:

- ``get``
- ``put``
- ``del`` *delete is a reserved word in javascript*
- ``post``

Essentially all the server and database instances do is cut down the number of characters you have to write to make a request to a Couch server.  Oh, that and parsing the response into something useful to.

For instance, if you were to call the following::

    db.get('test', function(err, resp) {
        console.log(resp);
    });
    
In the background, SuperComfy is making a GET request to ``http://localhost:5984/testdb/test`` and then parsing the result.

Similarly, the following would create a new document (if it didn't already exist) by sending through a PUT request to couch::

    db.put({ _id: 'newdoc' }, function(err, resp) {
    
    });