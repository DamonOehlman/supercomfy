# SuperComfy

It's the supercomfy CouchDB client built for both [Node.js](http://nodejs.org/) and browser use.  Most of the hard work is taken care of by the excellent [SuperAgent](https://github.com/visionmedia/superagent) module.

<a href="http://travis-ci.org/#!/DamonOehlman/supercomfy"><img src="https://secure.travis-ci.org/DamonOehlman/supercomfy.png" alt="Build Status"></a>

## Thanks

Thanks to the following people for supporting libraries, and/or inspiration through other CouchDB javascript clients:

- [TJ Holowaychuk](https://github.com/visionmedia) - For [superagent](https://github.com/visionmedia/superagent) and so many other great javascript libraries.

- [Nuno Job](https://github.com/dscape) - Great work on the wonderful and lightweight [Nano](https://github.com/dscape/nano) CouchDB client for NodeJS.