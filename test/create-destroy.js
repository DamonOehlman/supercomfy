var couch = require('./helpers/couch'),
    expect = require('chai').expect,
    testdb = 'testdb-' + new Date().getTime();

describe('database creation, info, list and remove', function() {
    it('should be able to create a database', function(done) {
        couch.create(testdb, done);
    });
    
    it('should be able to get info on the test db', function(done) {
        couch.use(testdb).info(done);
    });
    
    it('should be able to see the db in the list of databases on the server', function(done) {
        couch.list(function(err, dbs) {
            expect(dbs).to.exist;
            expect(dbs).to.include(testdb);

            done();
        });
    });
    
    it('should be able to remove a database', function(done) {
        couch.remove(testdb, done);
    });
    
    it('should not longer have the db in the list of databases on the server', function(done) {
        couch.list(function(err, dbs) {
            expect(dbs).to.exist;
            expect(dbs).to.not.include(testdb);
            
            done();
        });
    });
});