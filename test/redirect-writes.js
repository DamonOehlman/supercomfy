var couch = require('./helpers/couch'),
    expect = require('chai').expect,
    testdb = 'testdb-' + new Date().getTime(),
    redirectedDB = 'redirecteddb-' + new Date().getTime(),
    docId = 'test-' + new Date().getTime(),
    currentRev;
    
function checkDirect() {
    it('can create a document in the redirecteddb (prior to redirection)', function(done) {
        couch.use(redirectedDB).put({ _id: docId }, function(err, details) {
            expect(details).to.exist;
            expect(details.id).to.equal(docId);

            done();
        });
    });
    
    it('can check the document exists in the redirecteddb', function(done) {
        couch.use(redirectedDB).get(docId, function(err, details) {
            expect(details).to.exist;
            expect(details._id).to.equal(docId);
            
            done();
        });
    });
} // checkDirect

describe('redirected writes', function() {
    it('can create the testdb', function(done) {
        couch.create(testdb, done);
    });
    
    it('can create the redirected db', function(done) {
        couch.create(redirectedDB, done);
    });
    
    checkDirect();
    
    it('can update the test doc id', function() {
        // generate a new doc id
        var newId = 'test-' + new Date().getTime();
        
        expect(newId).to.not.equal(docId);
        docId = newId;
    });
    
    it('can redirect writes to the testdb', function() {
        couch.use(redirectedDB).redirectWrites(couch.use(testdb));
    });
    
    it('can write a document to the redirected db', function(done) {
        couch.use(redirectedDB).put({ _id: docId }, function(err, details) {
            expect(details).to.exist;
            expect(details.id).to.equal(docId);
            
            done();
        });
    });
    
    it('can not read the new document from the redirected db', function(done) {
        couch.use(redirectedDB).get(docId, function(err, details) {
            expect(err).to.exist;
            expect(err.reason).to.include('missing');
            
            done();
        });
    });
    
    it('can read the document from the testdb', function(done) {
        couch.use(testdb).get(docId, function(err, details) {
            expect(details).to.exist;
            expect(details._id).to.equal(docId);
            
            currentRev = details._rev;
            
            done();
        });
    });
    
    it('can delete the document from the redirected db', function(done) {
        couch.use(redirectedDB).del(docId, { rev: currentRev }, function(err, details) {
            expect(details).to.exist;
            expect(details.ok).to.be.truthy;
            
            done();
        });
    });
    
    it('has removed the document from the testdb', function(done) {
        couch.use(testdb).get(docId, function(err, details) {
            expect(err).to.exist;
            expect(err.reason).to.include('deleted');
            
            done();
        });
    });
    
    it('can cancel redirection by passing an empty target', function() {
        couch.use(redirectedDB).redirectWrites();
    });
    
    checkDirect();
    
    it('can remove the redirected db', function(done) {
        couch.remove(redirectedDB, done);
    });
    
    it('can remove the testdb', function(done) {
        couch.remove(testdb, done);
    });
});