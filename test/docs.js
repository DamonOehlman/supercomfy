var couch = require('./helpers/couch'),
    expect = require('chai').expect,
    dbid = 'testdb-' + new Date().getTime(),
    docId = 'test-' + new Date().getTime(),
    testdb,
    currentRev;

describe('document access and create tests', function() {
    before(function(done) {
        couch.create(dbid, function(err) {
            if (! err) {
                testdb = couch.use(dbid);
            }
            
            done(err);
        });
    });
    
    it('should be able to put a new document', function(done) {
        testdb.put({ _id: docId, name: 'testy test' }, function(err, doc) {
            expect(doc).to.exist;
            
            expect(doc.id).to.exist;
            expect(doc.id).to.equal(docId);
            
            expect(doc.rev).to.exist;
            
            // save the current revision
            currentRev = doc.rev;

            // flag done
            done();
        });
    });
    
    it('should be able to get the document', function(done) {
        testdb.get(docId, function(err, doc) {
            expect(doc).to.exist;
            expect(doc._id).to.equal(docId);
            expect(doc._rev).to.equal(currentRev);
            expect(doc.name).to.equal('testy test');
            
            done();
        });
    });
    
    it('should be able to update the document', function(done) {
        testdb.put({ _id: docId, _rev: currentRev, name: 'new testy test' }, function(err, doc) {
            expect(doc).to.exist;
            expect(doc.id).to.equal(docId);
            expect(doc.rev).to.exist;
            expect(doc.rev).to.not.equal(currentRev);
            
            // update the current revision
            currentRev = doc.rev;
            
            done();
        });
    });
    
    it('should be able to get the updated document', function(done) {
        testdb.get(docId, function(err, doc) {
            expect(doc).to.exist;
            expect(doc._id).to.equal(docId);
            expect(doc._rev).to.equal(currentRev);
            expect(doc.name).to.equal('new testy test');
            
            done();
        });
    });
    
    it('should be able to list the documents', function(done) {
        testdb.list(function(err, docs) {
            expect(docs).to.exist;
            expect(docs.rows.length).to.be.above(0);

            done();
        });
    });
    
    it('should not able to delete the document without the revision info', function(done) {
        testdb.del(docId, function(err, doc) {
            expect(err).to.exist;
            expect(err.reason).to.include('conflict');

            done();
        });
    });
    
    it('should be able to delete a document', function(done) {
        testdb.del(docId, { rev: currentRev }, function(err, doc) {
            expect(doc).to.exist;
            expect(doc.ok).to.be.truthy;
            
            done();
        });
    });
    
    it('should no longer be able to retrieve the document', function(done) {
        testdb.get(docId, function(err, doc) {
            expect(err).to.exist;
            expect(err.reason).to.include('deleted');

            done();
        });
    });
    
    after(function(done) {
        couch.remove(dbid, done);
    });
});