/* ~supercomfy~
 * 
 * Lightweight CouchDB client built on superagent
 * 
 * -meta---
 * version:    0.1.5
 * builddate:  2012-10-29T23:17:01.571Z
 * generator:  interleave@0.5.23
 * 
 * 
 * 
 */ 

// umdjs returnExports pattern: https://github.com/umdjs/umd/blob/master/returnExports.js
(function (root, factory) {
    if (typeof exports === 'object') {
        module.exports = factory(require('superagent'), require('events'), require('debug'));
    } else if (typeof define === 'function' && define.amd) {
        define(['superagent', 'events', 'debug'], factory);
    } else {
        root['supercomfy'] = factory(root['superagent'], root['events'], root['debug']);
    }
}(this, function (superagent, events, debug) {
    
    function CouchRequest(request) {
        var couchReq = this;
        
        // call the inherited constructor
        events.EventEmitter.call(this);
        
        // set headers
        
        request.end(function(res) {
            couchReq.parseResponse(res);
        });
    } // CouchRequest
    
    CouchRequest.prototype = new events.EventEmitter();
    CouchRequest.prototype.constructor = CouchRequest;
    
    CouchRequest.prototype.parseResponse = function(res) {
        
        var body, err;
    
        function _formatError(text) {
            
            var details, formatted;
    
            try {
                details = JSON.parse(text);
            }
            catch(e) {
            }
            
            if (details) {
                formatted = new Error(details.error);
                formatted.reason = details.reason;
            }
            else {
                formatted = new Error(text);
            }
            
            return formatted;
        }
        
        debug('received response', res);
        
        // if we don't have a response, then return
        if (!res) return;
        
        if (res.ok) {
            try {
                body = JSON.parse(res.text);
            }
            catch (e) {
                err = new Error('Unable to parse response');
            }
        }
        
        // if we have no status, then something went wrong
        if (! res.status) {
            err = new Error('No response');
        }
        
        this.emit(
            'end', 
            res.error ? _formatError(res.text) : err,
            body,
            res
        );
    };
    function CouchDB(server, dbname) {
        this.server = server;
        this.dbname = dbname;
    
        // initialise the method overrides
        // method overrides are used to redirect database level operations to a different couchserver
        // this is particularly useful when you have replication configured and want to direct write
        // operations to the parent server
        this._overrides = {};
    } // CouchDB
    
    ['get', 'post', 'put', 'del'].forEach(function(method) {
        CouchDB.prototype[method] = function(url, data, callback) {
            var overrideDB = this._overrides[method];
            
            // if the url parameter is actually data, then map arguments
            if (typeof url == 'object' && (! (url instanceof String))) {
                callback = data;
                data = url;
                
                url = data._id || '';
            }
    
            // if we have a db override for the specified method, then pass the request there
            if (overrideDB) {
                return overrideDB[method].call(overrideDB, url, data, callback);
            }
            // otherwise, pass on to our server
            else {
                return this.server[method].call(this.server, this.dbname + '/' + (url || ''), data, callback);
            }
        };
    });
    
    CouchDB.prototype.redirectWrites = function(targetDB) {
        var meshdb = this;
        
        if (targetDB) {
            debug('redirecting writes to: ' + targetDB.server.url + '/' + targetDB.dbname);
    
            ['put', 'del'].forEach(function(method) {
                meshdb._overrides[method] = targetDB;
            });
        }
        else {
            debug('resetting redirects');
            this._overrides = {};
        }
        
        return this;
    };
    
    CouchDB.prototype.info = function(callback) {
        this.server.get(this.dbname, callback);
    };
    
    CouchDB.prototype.list = function(opts, callback) {
        this.get('_all_docs', opts, callback);
    };
    var reTrailingSlash = /\/$/,
        _mappedMethods = {
            del: 'DELETE'
        };
    
    // ## CouchServer definition
    
    function CouchServer(url, opts) {
        this.url = (url || 'http://localhost:5984/').replace(reTrailingSlash, '') + '/';
        
        // ensure options have been provided
        opts = opts || {};
        
        // set server options
        this.jsonp = opts.jsonp;
        
        // initialise the db cache
        this._dbCache = {};
    }
    
    ['get', 'post', 'put', 'del'].forEach(function(method) {
        CouchServer.prototype[method] = function(url, data, callback) {
            var targetUrl, sa, request;
            
            // handle no url or data, just a callback
            if (typeof url == 'function' && typeof data == 'undefined') {
                callback = url;
                url = '';
            }
            // handle url but no data
            else if (typeof data == 'function' && typeof callback == 'undefined') {
                callback = data;
                data = undefined;
            }
            
            // ensure the target url is formatted correctly
            targetUrl = this.url.replace(reTrailingSlash, '') + '/' + url;
            
            // create the request
            debug('making ' + method + ' request to: ' + targetUrl);
            
            // create the super agent request
            sa = superagent(_mappedMethods[method] || method, targetUrl);
            
            // add data if we have some
            if (data) {
                sa[method == 'del' || method == 'get' ? 'query' : 'send'](data);
            } 
            
            // create the couch request
            request = new CouchRequest(sa);
            
            // handle the end response
            if (callback) {
                request.on('end', callback);
            }
            
            return request;
        };
    });
    
    CouchServer.prototype.create = function(dbname, callback) {
        this.put(dbname, {}, callback);
    };
    
    CouchServer.prototype.remove = function(dbname, callback) {
        this.del(dbname, callback);
    };
    
    CouchServer.prototype.use = function(dbname, flushCache) {
        if (!this._dbCache[dbname] || flushCache) {
            this._dbCache[dbname] = new CouchDB(this, dbname);
        }
        
        return this._dbCache[dbname];
    };
    
    CouchServer.prototype.list = function(opts, callback) {
        this.get('_all_dbs', opts, callback);
    };
    
    function supercomfy(serverUrl, opts) {
        return new CouchServer(serverUrl, opts);
    }
    
    return typeof supercomfy != 'undefined' ? supercomfy : undefined;
}));